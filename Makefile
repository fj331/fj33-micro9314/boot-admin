JAR_FILE=target/boot-admin-0.0.1-SNAPSHOT.jar
IMAGE_NAME=boot-admin


pipeline/docker/build:
	./mvnw clean package -DskipTests
	docker image build --build-arg JAR=${JAR_FILE} -t ${IMAGE_NAME} .